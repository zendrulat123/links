package main

import (
	"net/http"
)

//User pulls from database..
type User struct {
	UserID   int     `json:"id"`
	Username string  `json:"username"`
	Friends  []*User `json:"friends"`
}

//get handler
func me(w http.ResponseWriter, r *http.Request) {
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost")

	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	w.WriteHeader(http.StatusOK)

	w.Header().Set("Content-Type", "text/html")
	switch r.Method {
	case http.MethodGet:
		//3306
		//user phpmyadmin@localhost

		err := tpl.ExecuteTemplate(w, "me.html", nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}

		return
	}

}
